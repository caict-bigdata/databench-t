/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

public class TranlogCount {

    private String trancfg_type; //业务类型
    private Double total_cost; //总耗时
    private Double avg_cost; //平均耗时
    private Double max_cost; //最大耗时
    private Integer num; //总数
	public String getTrancfg_type() {
		return trancfg_type;
	}
	public void setTrancfg_type(String trancfg_type) {
		this.trancfg_type = trancfg_type;
	}
	public Double getTotal_cost() {
		return total_cost;
	}
	public void setTotal_cost(Double total_cost) {
		this.total_cost = total_cost;
	}
	public Double getAvg_cost() {
		return avg_cost;
	}
	public void setAvg_cost(Double avg_cost) {
		this.avg_cost = avg_cost;
	}
	public Double getMax_cost() {
		return max_cost;
	}
	public void setMax_cost(Double max_cost) {
		this.max_cost = max_cost;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}

    
}
