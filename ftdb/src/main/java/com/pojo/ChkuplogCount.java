/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

public class ChkuplogCount {

    private String chkuplog_type; //检查类型
    private String chkuplog_success; //对账编号
    private int num; //对账说明
	public String getChkuplog_type() {
		return chkuplog_type;
	}
	public void setChkuplog_type(String chkuplog_type) {
		this.chkuplog_type = chkuplog_type;
	}
	public String getChkuplog_success() {
		return chkuplog_success;
	}
	public void setChkuplog_success(String chkuplog_success) {
		this.chkuplog_success = chkuplog_success;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
    
    

}
