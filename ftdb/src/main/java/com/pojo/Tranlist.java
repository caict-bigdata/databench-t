/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;

public class Tranlist {

    private Date tranlist_date; //交易日期
    private String tranlist_branchid; //发生网点
    private Integer tranlist_seq; //流水号
    private Time tranlist_time; //交易时间
    private String tranlist_accountid1; //账号
    private BigDecimal tranlist_bale; //发生额
    private String tranlist_dcdir; //借贷方向
    private String tranlist_accountid2; //对方账号

    private String tranlist_fld1;//备用字段(目前转账业务插入交易码)
    
    private String tranlist_fld2;//备用字段(记录每次测试的id)
    
    private BigDecimal tranlist_fld3; //备用字段(目前记录存取款金额)

    public Date getTranlist_date() {
        return tranlist_date;
    }

    public void setTranlist_date(Date tranlist_date) {
        this.tranlist_date = tranlist_date;
    }

    public String getTranlist_branchid() {
        return tranlist_branchid;
    }

    public void setTranlist_branchid(String tranlist_branchid) {
        this.tranlist_branchid = tranlist_branchid;
    }

    public Integer getTranlist_seq() {
        return tranlist_seq;
    }

    public void setTranlist_seq(Integer tranlist_seq) {
        this.tranlist_seq = tranlist_seq;
    }

    public Time getTranlist_time() {
        return tranlist_time;
    }

    public void setTranlist_time(Time tranlist_time) {
        this.tranlist_time = tranlist_time;
    }

    public String getTranlist_accountid1() {
        return tranlist_accountid1;
    }

    public void setTranlist_accountid1(String tranlist_accountid1) {
        this.tranlist_accountid1 = tranlist_accountid1;
    }

    public BigDecimal getTranlist_bale() {
        return tranlist_bale;
    }

    public void setTranlist_bale(BigDecimal tranlist_bale) {
        this.tranlist_bale = tranlist_bale;
    }

    public String getTranlist_dcdir() {
        return tranlist_dcdir;
    }

    public void setTranlist_dcdir(String tranlist_dcdir) {
        this.tranlist_dcdir = tranlist_dcdir;
    }

    public String getTranlist_accountid2() {
        return tranlist_accountid2;
    }

    public void setTranlist_accountid2(String tranlist_accountid2) {
        this.tranlist_accountid2 = tranlist_accountid2;
    }

	public Tranlist(Date tranlist_date, String tranlist_branchid, Integer tranlist_seq, Time tranlist_time,
			String tranlist_accountid1, BigDecimal tranlist_bale, String tranlist_dcdir, String tranlist_accountid2,String tranlist_fld1,String tranlist_fld2,BigDecimal tranlist_fld3) {
		super();
		this.tranlist_date = tranlist_date;
		this.tranlist_branchid = tranlist_branchid;
		this.tranlist_seq = tranlist_seq;
		this.tranlist_time = tranlist_time;
		this.tranlist_accountid1 = tranlist_accountid1;
		this.tranlist_bale = tranlist_bale;
		this.tranlist_dcdir = tranlist_dcdir;
		this.tranlist_accountid2 = tranlist_accountid2;
		this.tranlist_fld1 = tranlist_fld1;
		this.tranlist_fld2 = tranlist_fld2;
		this.tranlist_fld3 = tranlist_fld3;

	}
	public Tranlist() {
		
	}
	public String getTranlist_fld1() {
		return tranlist_fld1;
	}

	public void setTranlist_fld1(String tranlist_fld1) {
		this.tranlist_fld1 = tranlist_fld1;
	}

	public String getTranlist_fld2() {
		return tranlist_fld2;
	}

	public void setTranlist_fld2(String tranlist_fld2) {
		this.tranlist_fld2 = tranlist_fld2;
	}

	public BigDecimal getTranlist_fld3() {
		return tranlist_fld3;
	}

	public void setTranlist_fld3(BigDecimal tranlist_fld3) {
		this.tranlist_fld3 = tranlist_fld3;
	}
	
	
    
}
