/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.pojo.Account;
import com.pojo.Branch;
import com.pojo.Customer;
import com.pojo.Sjno;
import com.pojo.Tranlist;
import com.pojo.Tranlog;

@Mapper
@Repository
public interface Transfer {

    void transferFrom(Account ac);

    void transferTo(Account ac);
    
    Account readAccount(String account_id);
    
    Account readTransactionalAccount(String account_id);
    
    void transferFromSjno(Sjno sj);

    void transferToSjno(Sjno sj);
    
    Branch readTransactionalBranchSeq(String branch_id);

    void insertTranlist(Tranlist tranlist);
    
    void updateBranch(String branch_id);

    void insertTranlog(Tranlog tranlog);

    void updateCustomerFrom(Customer ct);

    void updateCustomerTo(Customer ct);
}
